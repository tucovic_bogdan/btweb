import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()

export class PortfolioGalleryService {

	constructor ( private http: HttpClient ) { }
	
	getAllData() {
		return this.http
		.get('https://api.myjson.com/bins/19dbql')
			.map((res) => res);
	}

}

export interface galleryObject {
	id: number,
	imgUrl: string,
	title: string
}

