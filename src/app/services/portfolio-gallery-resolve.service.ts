import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { PortfolioGalleryService, galleryObject } from './portfolio-gallery.service';

export interface GalleryObj {
  id: number,
  imgUrl: string,
  title: string
}


@Injectable()

/**
 *  ruter ce pozvati metodu resolve kada korisnici posete putanju
 *  za koju vezeno resolve() metod
 *
 */

export class PortfolioGalleryResolve implements Resolve<GalleryObj[]> {

  allData: galleryObject[];

	constructor ( private service: PortfolioGalleryService ) { }

  resolve(route: ActivatedRouteSnapshot):Observable<GalleryObj[]>  {
    return Observable.create((observer: Observer<GalleryObj[]>) => {

      this.service.getAllData().subscribe(response => {
        if (Array.isArray(response)) {
          this.allData = response;
        } else {
          this.allData = [];
          console.error('Podaci galerije nisu dostupni');
        }
      });

      setTimeout(() => {
        observer.next(this.allData)
        observer.complete(); 
      }, 800);
    })

  }

}