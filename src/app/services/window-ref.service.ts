import { Injectable } from '@angular/core';

const _window = () : any => window;

@Injectable()

/**
 *  angular 2 nema window servis za razliku od angular-a 1.x
 *  nije dobra praksa koristiti window kao globalnu promenljivu te se stvara servis
 */
export class WindowRef {
  get nativeWindow() : any {
    return _window();
  }
}