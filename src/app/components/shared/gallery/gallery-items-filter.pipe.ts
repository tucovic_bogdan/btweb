import { Pipe, PipeTransform } from '@angular/core';
import { GalleryObj } from '../../../services/portfolio-gallery-resolve.service';

@Pipe({name: 'GalleryItemsFilter'})

// Pipe za filtriranje prikaza slika prema broju
export class GalleryItemsFilterPipe implements PipeTransform {
  transform(value: GalleryObj[], length: number | 'total'): GalleryObj[] {
  	let result:GalleryObj[] = [];
   	
   	if (typeof length === 'number') {
   		result = Array.from(value.filter((item,index) => index < length));
		} else if (length === 'total') {
			result = Array.from(value);
		}

    return result;
  }
}