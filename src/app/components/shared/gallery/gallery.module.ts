import { NgModule } from '@angular/core'; 
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { GalleryComponent } from './gallery.component'; 
import { GalleryOpenImgComponent } from './gallery-open-img.component';
import { GalleryItemDirective } from './gallery-item.directive';
import { GalleryItemsFilterPipe } from './gallery-items-filter.pipe';
import { GalleryConfig } from './gallery-config';

let elements = [ 
	GalleryComponent,
	GalleryItemDirective,
	GalleryOpenImgComponent,
	GalleryItemsFilterPipe
];

@NgModule({
	imports: [CommonModule, RouterModule],
	declarations: [...elements],
	exports:[...elements],
	providers:[GalleryConfig]
})

export class GalleryModule { }