import { 
	AfterContentInit,
	Component, 
	ContentChildren,
	Input,
	OnInit,
	Renderer2,
	QueryList
} from '@angular/core';

import { GalleryItemDirective } from './gallery-item.directive';
import { GalleryObj } from '../../../services/portfolio-gallery-resolve.service';
import { GalleryConfig } from './gallery-config';

@Component({
	selector: 'gallery',
	templateUrl: './gallery.component.html',
	styleUrls: ['./gallery.component.css']
})

export class GalleryComponent implements OnInit, AfterContentInit {

	/**
	 *  broj kolona za prikaz slika pri rezoluciji vecoj od 992px (Bootstrap md)
	 */ 
	@Input() columns? : 2 | 3 | 4 ;

	// postojanje razmaka izmedju slika
	@Input() isSpace?: boolean;

	/**
	 *  broj slika koji ce biti prikazan
	 *  vrednost 'total' oznacava da nece postojati dugme za povecanje
	 *	broja prikazanih slika jer inicijalno sve slike biti prikazane
	 */
	@Input() itemsLength?:  number | 'total';

	/**
	 *  vrednost uvecanja broja prikazanih slika za svaki klik na dugme za uvecanje
	 */
	@Input() itemsIncrement?: number;

	@ContentChildren(GalleryItemDirective) items: QueryList<GalleryItemDirective>;

	// id otvorene, ekspandovane slike
	activeId: number | false;

	// postojanje dugmeta za uvecanje broja prikazanih slika
	isShowMore: boolean;

	// ukupan broj dostupnih slika
	total: number;

	constructor (
		private config: GalleryConfig,
		private  renderer: Renderer2
	) {}

	/**
	 * nedefinisana input svojstva dobijaju vrednost iz config servisa
	 */
	ngOnInit() {
		const elements = [
			'columns',
			'isSpace',
			'itemsIncrement',
			'itemsLength'
		];

		for (let el of elements) {
			if (typeof this[el] === 'undefined') {
				this[el] = this.config[el];
			}	
		}

		this.isShowMore = typeof this.itemsLength === 'number' ? true : false;

	}

	/**
	 *  @return - bootstrap klasa kolona
	 */
	getItemColumnClass():string {
		let className = '';

		switch(this.columns) {
			case 4:
				className = 'col-md-3 col-sm-6';
			break;
			case 3:
				className = 'col-md-4 col-sm-6';
				break;
			case 2:
				className = 'col-md-6';
				break;
		}

		return className;
	}

	ngAfterContentInit() {
		this.total = this.items.length;
	}

	showIcons(event, newActiveId : number) {
		const captionHeight = event.target.lastElementChild.clientHeight;
		const transitionDuration = 300;
		
		const imgEl = event.target.firstElementChild;
		const captionEl = event.target.lastElementChild;

		this.renderer.setStyle(imgEl, 'transform', `translateY(-${captionHeight}px)`);
		this.renderer.setStyle(imgEl, 'transition', `transform ${transitionDuration}ms ease-out`);
		
		this.renderer.setStyle(captionEl, 'transform', `translateY(-${captionHeight}px)`);
		this.renderer.setStyle(captionEl, 'transition', `transform ${transitionDuration}ms ease-out`);

		// prikazivanje ikonice nakon zavrsene tranzicije
		setTimeout(() => {
			this.activeId = newActiveId;
		}, transitionDuration);
		
	}

	hideIcons(event) {
		const imgEl = event.target.firstElementChild;
		const captionEl = event.target.lastElementChild;

		this.renderer.setStyle(imgEl, 'transform', `translateY(0)`);
		this.renderer.setStyle(captionEl, 'transform', `translateY(0)`);
		this.activeId = false;
	}

	// metoda za uvecanje broja prikazanih slika
	showMore() {
		if (typeof this.itemsLength === 'number') {

			this.itemsLength += this.itemsIncrement;
			
			if (this.itemsLength >= this.total) {
				this.isShowMore = false;
			}

		}
	}
}




