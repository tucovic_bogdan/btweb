import {
	Directive,
	Input
} from '@angular/core';

@Directive({selector: 'galleryitem'})

export class GalleryItemDirective {

	@Input() title: string;
	@Input() imgUrl: string;
	@Input() id: number;

}