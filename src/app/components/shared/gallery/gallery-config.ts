import { Injectable } from '@angular/core';

@Injectable()

export class GalleryConfig {
	
	columns: 2 | 3 | 4 = 3;

	isSpace: boolean = false;

	itemsLength: number | 'total' = 5;

	itemsIncrement: number = 5;

}