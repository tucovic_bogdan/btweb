import { 
	Component, 
	OnInit
} from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { GalleryObj } from '../../../services/portfolio-gallery-resolve.service';
import { expandAnimation } from '../../../animations/';

@Component({
	templateUrl: "./gallery-open-img.component.html",
	styleUrls: ['./gallery-open-img.component.css'],
	animations: [ expandAnimation ]
})

/**
 *  komponenta za ekspandovanje targetirane slike iz galerije
 *  koriscenjem resolvera umesto direktnog ubrizgavanja servisa 
 *  pruzena je mogucnost visestruke upotrebe komponente
 */

export class GalleryOpenImgComponent implements OnInit {

	/**
	 *  objekat otvorene,prosirene slike
	 */
	imgData : GalleryObj;

	/**
	 * niz objekata dostupnih slika galerije
	 */
	galleryData: GalleryObj[];
	
	/**
	 *  vrednost id svojstva objekta imgData i  
	 *  vrednost route parametra id
	 */
	activeId : number;

	/**
	 *  najmanja vrednost id svojstva medju objektima galleryData niza
	 *  potrebna za uslov onemogucenja 'prev' dugmeta, 
	 *  a najveca za onemogucavanje 'next' dugmeta
	 */ 
	minId: number;
	maxId: number;

	constructor ( 
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private location: Location
	) {}

	ngOnInit() {
		this.galleryData = this.activatedRoute.snapshot.data.galleryItems;
		
		let idsArray : number[] = [];
		this.galleryData.forEach(item => {
			idsArray.push(item.id);
		})
		this.minId = Math.min( ...idsArray);
		this.maxId = Math.max( ...idsArray);

	 	this.activatedRoute.params.subscribe(params => {
	 		this.changeImg(+params['id']);
		});

	 	// fokusiranje na otvorenu sliku preko sidro linka
		this.router.events.subscribe(s => {
      const tree = this.router.parseUrl(this.router.url);
      
      if (tree.fragment) {
          const element = document.querySelector(`#${tree.fragment}`);
          element && element.scrollIntoView(element);
        }
      
    });
	}

	changeImg(id : number) {
		this.activeId = id;
		this.imgData = this.galleryData.find(item => item.id === id);
	}


	manualChangeImg(newId: number) {
		let pathArray = this.location.path().split('/');
		pathArray.pop();
		let relativePath = pathArray.join('/');

		this.router.navigate([relativePath, newId], {fragment: 'slika'});
	}

}





