import { Injectable } from '@angular/core';

@Injectable()

export class AccordionConfig {
	
	// da li ce postojati ikonice uz naslov
	isIcons: boolean = true; 

	// identifikatori panela koji ce biti otvoreni u startu
	activeIds: number[] = [1];

	// da li ce se zatvoriti vec otvoreni paneli pri otvaranju novog panela
	closeOthers: boolean = false;

}