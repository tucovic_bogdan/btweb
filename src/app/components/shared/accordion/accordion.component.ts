import { 
	Component, 
	ContentChildren,
	EventEmitter,
	Input,
	Output,
	OnInit,
	Renderer2,
	QueryList
} from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

import { PanelDirective } from './panel.directive';
import { AccordionConfig } from './accordion-config';

@Component({
	selector: 'accordion',
	templateUrl: './accordion.component.html',
	styleUrls: ['./accordion.component.css'],
	animations: [
		trigger('contentAnimation', [
      state('true' , style({ maxHeight: '500px'})),
      state('false', style({ maxHeight: '0'  })),
      transition('* <=> *', animate('300ms')),
		])
	]
})


export class AccordionComponent {
	/**
	 *  identifikator aktivnog(otvorenog panela)
	 */ 
	@Input() activeIds?: number[];
	
	/**
	 *  u css-u moze se dodati bilo koja klasa modifikator
	 */
	@Input() classModifier?: string; 

	/**
	 *  logicka vrednost od koje zavisi da li ce se vec otvoreni paneli zatvoriti 
	 *  pri otvaranju novog panela 
	 */
	@Input() closeOthers?: boolean;

	/**
	 *  da li ce uz naslov postojati Google ikonice
	 *  kao indikatori statusa aktivnosti panela
	 */
	@Input() isIcons?: boolean;

	/**
	 *
	 * dogadjaj koji se emituje pre nego sto se panel otvori/zatvori
	 *
	 */
	@Output() panelChange = new EventEmitter<PanelChangeEvent>();
	
	@ContentChildren(PanelDirective) panels: QueryList<PanelDirective>;

	constructor(protected config: AccordionConfig) {}

	ngOnInit() {

		// vrednosti nedefinisanih inputa dodeljuju se iz config provajdera

		if (typeof this.isIcons === 'undefined') {
			this.isIcons = this.config.isIcons;
		}

		if (typeof this.closeOthers === 'undefined') {
			this.closeOthers = this.config.closeOthers;
		}

		if (typeof this.activeIds === 'undefined') {
			this.activeIds = Array.from(this.config.activeIds);
		}
	}
	
	 //  @return  true - otvoren | false - zatvoren panel

	isActive(panelId: number):boolean {
		return this.activeIds.indexOf(panelId) >= 0;
	}

	closeAll():void {
		this.activeIds.length = 0;
	}

	toggle(event: MouseEvent, panelId: number):void {
		event.preventDefault();

		let action;
		/**
		 *  panel je zatvoren ako se vrednost njegovog 
		 *  id svojstva ne nalaziu u nizu activeIds
		 */
		const ind = this.activeIds.indexOf(panelId);
		const isClosed = ind < 0;
		
		if (isClosed) {
			if (this.closeOthers) this.closeAll();
			this.activeIds.push(panelId);
			action = true;
		} else {
			this.activeIds.splice(ind, 1);
			action = false;
		}

		this.panelChange.emit({
			panelId, 
			action
		});
		
		this.isActive(panelId);
	}

}

interface PanelChangeEvent {
	/**
	 *  id panela nad kojim se izvodi akcija otvaranje/zatvaranje
	 */ 
	panelId: number,

	/**
	  *  logicka vrednost koja pokazuje da li se targetirani panel
	  *  otvara ili zatvara
	  */
	action: boolean
}



