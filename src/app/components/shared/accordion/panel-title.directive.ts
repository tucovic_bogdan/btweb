import {
	Directive,
	TemplateRef
} from '@angular/core';

@Directive({selector: 'ng-template[paneltitle]'})

export class PanelTitleDirective {
  constructor(public templateRef: TemplateRef<any>) {}
}