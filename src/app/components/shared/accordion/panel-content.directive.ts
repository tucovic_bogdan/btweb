import {
	Directive,
	TemplateRef
} from '@angular/core';

@Directive({selector: 'ng-template[panelcontent]'})

export class PanelContentDirective {
  constructor(public templateRef: TemplateRef<any>) {}
}