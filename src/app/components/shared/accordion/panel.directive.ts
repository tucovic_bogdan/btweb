import {
	Input,
	Directive,
	ContentChild,
	TemplateRef
} from '@angular/core';

import { PanelTitleDirective } from './panel-title.directive';
import { PanelContentDirective } from './panel-content.directive';

@Directive({selector: 'panel'})

export class PanelDirective {
	
	@Input() id: number;

	@ContentChild(PanelTitleDirective, {read: TemplateRef}) TitleTemplate: PanelTitleDirective;
	@ContentChild(PanelContentDirective, {read: TemplateRef}) ContentTemplate: PanelContentDirective;
}