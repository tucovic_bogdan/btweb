import { NgModule } from '@angular/core'; 
import { CommonModule } from '@angular/common';

import { AccordionConfig } from './accordion-config';
import { AccordionComponent } from './accordion.component'; 

import { PanelDirective } from './panel.directive';
import { PanelTitleDirective } from './panel-title.directive';
import { PanelContentDirective } from './panel-content.directive';

const elements = [ 
	AccordionComponent,
	PanelDirective,
	PanelContentDirective,
	PanelTitleDirective
];

@NgModule({
	imports: [CommonModule],
	declarations: [...elements],
	exports:[...elements],
	providers:[AccordionConfig]
})

export class AccordionModule { }