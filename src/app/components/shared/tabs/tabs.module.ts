import { NgModule } from '@angular/core'; 
import { CommonModule } from '@angular/common';

import { TabsConfig } from './tabs-config';
import { TabsComponent } from './tabs.component';
import { TabDirective } from './tab.directive';
import { TabTitleDirective } from './tab-title.directive';
import { TabContentDirective } from './tab-content.directive';

const elements = [
	TabsComponent,
	TabDirective,
	TabTitleDirective,
	TabContentDirective
];

@NgModule({
	imports: [CommonModule],
	declarations: [...elements],
	exports:[...elements],
	providers:[TabsConfig]
})

export class TabsModule { }