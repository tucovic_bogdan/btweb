import { 
	AfterContentInit,
	Component, 
	ContentChildren,
	Input,
	ElementRef,
	OnInit,
	QueryList
} from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

import { TabsConfig } from './tabs-config';
import { TabDirective } from './tab.directive';

@Component({
	selector: 'tabs',
	templateUrl: './tabs.component.html',
	styleUrls: ['./tabs.component.css'],
	animations: [
		trigger('tabContent', [
  		state('true' , style({ transform: 'translateX(0)' })),
      state('false', style({ transform: 'translateX(-100%)', display: 'none'})),
      transition('0 => 1', animate('500ms'))
		])
	]
})


export class TabsComponent implements OnInit {

	/**
	 *  opciona klasa, modifikator komponente 
	 */
	@Input() classModifier?: string;

	/**
	 *  id aktivnog taba
	 */
	@Input() selectedId?: number;

	@ContentChildren(TabDirective) tabs:QueryList<TabDirective>;

	constructor(protected config: TabsConfig ) {}

	ngOnInit() {
		if (typeof this.selectedId === 'undefined') {
			this.selectedId = this.config.selectedId;
		}
	}

	setActiveTab(newIndex: number, event):void {
		event.preventDefault();

		if (this.selectedId !== newIndex) {
			this.selectedId = newIndex;
		}
	}

}




