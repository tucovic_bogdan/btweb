import {
	Directive,
	TemplateRef
} from '@angular/core';

@Directive({selector: 'ng-template[TabTitle]'})

export class TabTitleDirective {
  constructor(public templateRef: TemplateRef<any>) {}
}