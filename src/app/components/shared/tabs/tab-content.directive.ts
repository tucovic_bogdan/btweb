import {
	Directive,
	TemplateRef
} from '@angular/core';

@Directive({selector: 'ng-template[TabContent]'})

export class TabContentDirective {
  constructor(public templateRef: TemplateRef<any>) {}
}