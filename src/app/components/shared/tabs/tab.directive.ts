import {
	Input,
	Directive,
	ContentChild,
	TemplateRef
} from '@angular/core';

import { TabTitleDirective } from './tab-title.directive';
import { TabContentDirective } from './tab-content.directive';

@Directive({selector: 'tab'})

export class TabDirective {
	@Input() id: number;

	@ContentChild(TabTitleDirective, {read: TemplateRef}) TitleTemplate: TabTitleDirective;
	@ContentChild(TabContentDirective, {read: TemplateRef}) ContentTemplate: TabContentDirective;
}