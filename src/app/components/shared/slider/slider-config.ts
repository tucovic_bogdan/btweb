import { Injectable } from '@angular/core';

@Injectable()


export class SliderConfig {

	/**
	 *  indeks slajda koji ce prvi biti prikazan
	 */ 
	startInd: number | string = 0; 

	/**
	 *   redosled prelaska slajdova pri auto rezimu
	 *  'prev' zadaje smenjivanje obrnutim redosledom npr. sa 5 na 4, 
	 *	 a 'next' suprotno
	 */
	direction: 'next' | 'prev' = 'prev' ;

	/**
	 *  oderdjuje postojanje strelica za manuelnu promenu slajdova
	 */
	hasArrows: boolean = true;

	/**
	 *  oderdjuje postojanje indikator ikonica za promenu slajdova
	 */
	hasIndicators: boolean = false;

	/**
	 *  interval u miliseknudama do promene slajda,
	 *  0 onemogucava auto promenu slajdova
	 */
	interval: number = 0;

	/**
	 *  zastavljanje prelaska kao odgovor na mouseEnter dogadjaj
	 */
	isHoverPause: boolean = true;

	// omogucavanje promene slajdova pomocu strelica arrowLeft, arrowRight
	isKeyChangeSlide : boolean = true;

}

