import { 
	AfterViewInit,
	Component, 
	ContentChild,
	ViewChild,
	ElementRef,
	Renderer2, 
	Input,
	Output,
	OnInit,
	OnDestroy,
	HostListener,
	EventEmitter,
	TemplateRef
} from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

import { SliderConfig } from './slider-config';
import { Slide } from './slide.directive';

@Component({
	selector: 'slider',
	templateUrl: './slider.component.html',
	styleUrls: ['./slider.component.css'],
	animations: [
		trigger('imgAnimation', [
			transition('* <=> *', [
			  style({
			    transform: 'scale(0.95)'
			  }),
			  animate('500ms ease-in', style({
			    transform: 'scale(1)'
			  }))
			])
		]),
		trigger('controlsAnimation', [
      state('true' , style({ transform: 'translateY(0)' })),
      state('false', style({ transform: 'translateY(200px)'  })),
      transition('* <=> *', animate('300ms')),
		])
	],
	host: {
		'(mouseenter)': 'mouseEnterHandler()',
		'(mouseleave)': 'mouseLeaveHandler()'
	}
})


export class SliderComponent implements OnInit, AfterViewInit, OnDestroy {
	
	@Input() hasArrows?: boolean;

	@Input() hasIndicators?: boolean;

	/**
	 *  interval odredjuje vremenski period do sledeceg slajda
	 */ 
	@Input() interval?: number; 

	/**
	 *  
	 *  smer promene slajdova prema indeksu u nizu
	 *  'prev'(unazad) | 'next'(unapred)
	 *
	 */
	@Input() direction?: 'prev' | 'next';
	
	/**
	 *  isHoverPause svojstvo odredjuje da li ce se auto 
	 *  zamena slajdova izvrsavati pri mouseEnterHandler dogadaju
	 */
	@Input() isHoverPause?: boolean;

	/**
	 *  identifikator trenutno prikazanog(aktivnog) slajda
	 */
	@Input() currentSlideInd: number;

	/**
	 *  pokazatelj ukupnog broja slajdova
	 */
	@Input() total: number; 

	@Input() isKeyChangeSlide?: boolean;
	
	/**
	 * slideChange dogadjaj se ispaljuje pri pokretanju akcije promene slajda
	 */
	@Output() slideChange = new EventEmitter<slideChangeEvent>();

	@ContentChild(Slide, {read: TemplateRef}) SlideTemplate: Slide;
	
	@ViewChild("controls", {read: ElementRef}) controls: ElementRef;

	@HostListener('window:keydown', ['$event']) 
		onKey(event: KeyboardEvent) { 
			if (this.isKeyChangeSlide) {
				if (event.key ===  'ArrowLeft') {
					this.prev();
				} else if (event.key ===  'ArrowRight') {
					this.next();
				}
			}
		}

	// referenca setInterval metode
	slideInterval: number | null = null;

	controlsDisplay: boolean;

	constructor(
		private config: SliderConfig,
		private renderer: Renderer2,
		private el: ElementRef
	) { }


	ngOnInit():void { 

		this.setDefaultInputs();

		/**
		 *
		 *  1. kontrole za manuelnu promenu slajdova su nedostupne ako postoji samo 1 slajd
		 *  2. interval sa vrednoscu 0 onemogucava auto promenu slajdova
		 *
		 */ 
		if (this.total === 1) {
			this.hasArrows = false; // 1
			this.hasIndicators = false; // 1
		} else if (this.total > 1) {
			if (this.interval > 0) { // 2
				this.auto();
			} 
		}

	}

	/**
	 *  kontrole treba da budu vidljive tek nakon mouseEnter dogadjaja
	 *  zbog cega ih inicijalno skrivamo uz pomoc css-a
	 */ 
	ngAfterViewInit(): void {
		this.renderer.setStyle(this.controls.nativeElement, 'transform', 'translateY(200px)');
  }

	/**
		*  vrednost nedefinisanih inputa dodeljuje  
		*  se iz config provajdera
		*
	*/
	setDefaultInputs():void {
		const elements: string[] = [
			'hasArrows',
			'hasIndicators',
			'direction',
			'interval',
			'isHoverPause',
			'isKeyChangeSlide'
		]

		for (let el of elements) {
			if (typeof this[el] === 'undefined') {
				this[el] = this.config[el];
			}
		}

	}

	/**
	 * @param { newSLideIndex } pozicija, slajda koji treba prikazati kao sledeci
	 *
	 */
	selectSlide(newSlideIndex: number):void {
		this.slideChange.emit({
			nextIndex: newSlideIndex
		});
	}

	mouseEnterHandler():void {
		this.controlsDisplay = true;

		if (this.isHoverPause && this.slideInterval !== null) {
			this.stop();
		}
	}

	mouseLeaveHandler():void {
		this.controlsDisplay = false;
		
		if (this.slideInterval === null && this.interval > 0 && this.total > 1) {
			this.auto();
		}
	}

	/**
	 *  metoda za auto zamenu slajdova u zadatom smeru i intervalu
	 */
	auto():void {
		this.slideInterval = setInterval(() => {
			(this.direction === 'next') && this.next();
			(this.direction === 'prev') && this.prev();
		}, this.interval);
	}

	/**
	 *  metoda za prelazak na naredni slajd
	 *
	 */
	next():void {
		let index = this.currentSlideInd;
		const isLastSlide = this.total - 1;

		if (index === isLastSlide) {
			index = 0;
		} else {
			index ++;
		}

		this.selectSlide(index);
	}

	prev():void {
		let index = this.currentSlideInd;
		const isFirstSlide = index === 0;
		
		if (isFirstSlide) {
			index = this.total - 1;
		} else {
			index --;
		}

		this.selectSlide(index);
	}


	stop():void {
		clearInterval(this.slideInterval);
		this.slideInterval = null;
	}

	// @return - niz brojeva za generisanje indikator ikonica
	getIndicatorsArr():number[] {
		return Array.from({length: this.total}, (v, i) => i);
	}

	ngOnDestroy():void {
		// zaustavljanje intervala radi ustede memorije
		clearInterval(this.slideInterval);
	}

}

interface slideChangeEvent {

	// indeks novog slajda
	nextIndex: number
}
