import { NgModule } from '@angular/core'; 
import { CommonModule } from '@angular/common';

import { SliderComponent } from './slider.component'; 
import { SliderConfig } from './slider-config';
import { Slide } from './slide.directive';

const elements = [ SliderComponent, Slide ];

@NgModule({
	imports: [CommonModule],
	declarations: [...elements],
	exports:[...elements],
	providers:[SliderConfig]
})

export class SliderModule { }