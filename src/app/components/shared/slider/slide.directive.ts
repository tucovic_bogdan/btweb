import {
	Directive,
	TemplateRef
} from '@angular/core';

@Directive({selector: '[slide]'})

export class Slide {
  constructor(public templateRef: TemplateRef<any>) {}
}