import { NgModule } from '@angular/core'; 
import { CommonModule } from '@angular/common';

import { CounterComponent } from './counter.component';
import { CounterItem } from './counter-item.directive'; 
import { CounterConfig } from './counter-config';

const elements = [
	CounterComponent,
	CounterItem
];

@NgModule({
	imports: [CommonModule],
	declarations: [...elements],
	exports:[...elements],
	providers:[CounterConfig]
})

export class CounterModule { }