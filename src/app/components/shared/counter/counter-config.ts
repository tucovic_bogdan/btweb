import { Injectable } from '@angular/core';

@Injectable()


export class CounterConfig {

	// vreme u miliseknudama za koje ce se izvrsiti promena brojeva
	changeValueDuration: number = 10000;

	// ofset kod iteracije koja se aktivira kao odgovor na scroll dogradjaj
	offset: number = 150;

	/**
	 *  'on-load' rezim kao okidac iteracije ima load dogadjaj,
	 *  a 'on-scroll' rezim scroll dogadjaj
	 */
	mode: 'on-load' | 'on-scroll' = 'on-scroll' ;

}

