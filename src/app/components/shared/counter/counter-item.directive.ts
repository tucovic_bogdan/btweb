import { 
	Directive, 
	Input
} from '@angular/core';

@Directive({
	selector: 'counterItem'
})

export class CounterItem {
	@Input() title: string;
	@Input() number: number;
}