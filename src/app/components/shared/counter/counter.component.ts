import { 
	AfterContentInit,
	AfterViewInit,
	Component, 
	ContentChildren,
	ElementRef,
	Input,
	OnInit,
	Renderer2,
	QueryList
} from '@angular/core';

import { WindowRef } from '../../../services/window-ref.service';
import { CounterConfig } from './counter-config';
import { CounterItem } from './counter-item.directive';

@Component({
	selector: 'counter',
	templateUrl: './counter.component.html',
	styleUrls: ['./counter.component.css']
})


export class CounterComponent implements AfterContentInit, AfterViewInit, OnInit {

	// opciona modifikator klasa
	@Input() classModifier?: string;

	@Input() changeValueDuration?: number;

	@Input() offset?: number;

	@Input() mode?: 'on-load' | 'on-scroll';

	@ContentChildren(CounterItem) counterItems:QueryList<CounterItem>;

	private numbers : number[] = [];

	constructor(
		private config: CounterConfig, 
		private elementRef: ElementRef, 
		private renderer: Renderer2,
		private winRef: WindowRef
	) {}

	ngOnInit() {
		const elements = [
			'changeValueDuration',
			'offset',
			'mode'
		];

		for (let el of elements) {
			if (typeof this[el] === 'undefined') {
				this[el] = this.config[el];
			}
		}
	}

	ngAfterContentInit() {
		this.counterItems.forEach((item) => {
			this.numbers.push(item.number);
			item.number = 0;
		});
	}

	ngAfterViewInit() {
		if (this.mode === 'on-load') {
			this.iterate();
		} else if (this.mode === 'on-scroll') {
			this.onScrollIterate();
		}
	}

	onScrollIterate():void {
		const windowHeight: number = this.winRef.nativeWindow.innerHeight;
		const counterOffsetTop: number = this.elementRef.nativeElement.offsetTop;
		
		let isStartIterator: boolean = false;
		let scrollTop: number;

		this.renderer.listen('window', 'scroll', (event) => { 
			scrollTop = event.target.scrollingElement.scrollTop;
			if (!isStartIterator && scrollTop > counterOffsetTop - windowHeight + this.offset) {
				isStartIterator = true;
				this.iterate();
			}
		});
	}

	iterate():void {
		this.counterItems.forEach((item, index) => {
			const delay = this.changeValueDuration / this.numbers[index];

			const interval = setInterval(() => { 

				if (item.number == this.numbers[index]) {
				 clearInterval(interval);  
				} else {
					item.number++ ;
				}

			}, delay);
		})
	}

}


