import { NgModule } from '@angular/core'; 
import { CommonModule } from '@angular/common';

import { ProgressbarComponent } from './progressbar.component';
import { ProgressbarConfig } from './progressbar-config';

const elements = [
	ProgressbarComponent
];

@NgModule({
	imports: [CommonModule],
	declarations: [...elements],
	exports:[...elements],
	providers:[ProgressbarConfig]
})

export class ProgressbarModule { }