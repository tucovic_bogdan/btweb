import { 
	AfterContentInit,
	AfterViewInit,
	Component, 
	Input,
	ElementRef,
	OnInit,
	Renderer2
} from '@angular/core';

import { WindowRef } from '../../../services/window-ref.service';
import { ProgressbarConfig } from './progressbar-config';

@Component({
	selector: 'progressbar',
	templateUrl: './progressbar.component.html',
	styleUrls: ['./progressbar.component.css']
})

/**
 *  'animated' rezim omogucava da pocne uvecavanje vrednosti 
 *   kao odgovor na scroll dogradjaj
 *  'animated' rezim ima smisla samo ako se komponenta ne nalazi u visini prozora
 */
export class ProgressbarComponent implements OnInit, AfterContentInit, AfterViewInit {

	/**
	 *  vrednost izmedju 0 i 100, vrednost progres bara
	 */
	@Input() value: number;

	@Input() title: 'string';

	@Input() mode?: 'static' | 'animated';

	
	/**
	 *  vreme u milisekundama za koje se vrednost uveca za 1 %
	 *  nepotrebna vrednost za 'static' rezim
	 */  
	@Input() changeValueInterval? : number;

	/**
	 *  ofset za pocetak intervala pri skrolovanju
	 *  nepotrebna vrednost za 'static' rezim
	 */ 
	@Input() offset?: number;

	targetValue: number;

	constructor (
		private config: ProgressbarConfig, 
		private renderer: Renderer2, 
		private elementRef: ElementRef,
		private winRef: WindowRef
	) {}

	ngOnInit() {
		if (typeof this.mode === 'undefined') {
			this.mode = this.config.mode;
		}

		if (this.mode === 'animated') {
			const elements = [
				'changeValueInterval',
				'offset'
			];

			for (let el of elements) {
				if (typeof this[el] === 'undefined') {
					this[el] = this.config[el];
				}
			}
		}
	}

	ngAfterContentInit():void {
		if (this.mode === 'animated') {
			this.targetValue = this.value;
			this.value = 0;
		}
	}

	ngAfterViewInit():void {
		this.mode === 'animated' && this.selectIteratorType();
	}

	selectIteratorType():void {
		const progressbarOffsetTop: number = this.elementRef.nativeElement.offsetTop;
		const windowHeight: number = this.winRef.nativeWindow.innerHeight;

		// ako je progresbar pozicioniran u visini prozora onda iteracija pocinje bez skrolovanja
		if (progressbarOffsetTop < windowHeight) {
			this.iterate();
		} else {
			this.onScrollIterate(progressbarOffsetTop, windowHeight);
		}
	}

	iterate():void {
		const interval = setInterval(() => {
			this.value == this.targetValue ? clearInterval(interval) : this.value++ ;
		}, this.changeValueInterval);
	}

	onScrollIterate(progressbarOffsetTop : number, windowHeight : number ):void {
		let scrollTop: number;
		let isStartIterator = false;
		
		this.renderer.listen('window', 'scroll', (event) => { 
			scrollTop = event.target.scrollingElement.scrollTop;
			if (!isStartIterator && scrollTop > progressbarOffsetTop - windowHeight + this.offset) {
				isStartIterator = true;
				this.iterate();
			}
			
		});
	}
}


