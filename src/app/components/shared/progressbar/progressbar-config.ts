import { Injectable } from '@angular/core';

@Injectable()


export class ProgressbarConfig {
	
	changeValueInterval: number = 50;

	mode: 'static' | 'animated' = 'animated';

	offset: number = 200;

}

