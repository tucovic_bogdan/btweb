import { 
	Component, 
	Input,
	OnInit,
	OnDestroy
} from '@angular/core';

import { NgIf } from '@angular/common';

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
	selector: 'contact-form',
	templateUrl: './contact-form.component.html',
	styleUrls: ['./contact-form.component.css']
})


export class ContactFormComponent {

	// svojstva za textNode labela ili vrednosti paceholder atributa
	captionFirstName: string = 'Ime *';
	captionLastName: string = 'Prezime *';
	captionEmail: string = 'Email *';
	captionTitle: string = 'Naslov ';
	captionMessage: string = 'Poruka *';

	submitted : boolean = false;

	formData: formData = {
		firstName: '',
		lastName: '',
		email: '',
		title: '',
		message: ''
	};

	@Input() placeholderOrLabel?: 'placeholder' | 'label'; 

	onSubmit(event) {
		this.submitted = true;
	}

	ngOnInit() {
		if (typeof this.placeholderOrLabel === 'undefined') {
			this.placeholderOrLabel = 'label';
		}
	}

}

interface formData {
	firstName: string,
	lastName: string,
	email: string,
	title: string,
	message: string
}





