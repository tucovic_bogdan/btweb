import { 
	Component, 
	Input,
	OnInit
} from '@angular/core';

import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

import { fadeInAnimation } from '../../../animations/index';
import { ContactFormComponent } from './contact-form/contact-form.component';

@Component({
	templateUrl: './contact.component.html',
	styleUrls: ['./contact.component.css'],
	animations: [fadeInAnimation],
	host: { '[@fadeInAnimation]': '' }
})


export class ContactComponent implements OnInit {

	constructor( 
		private titleService: Title,
		private activatedRoute: ActivatedRoute 
	) { }

	ngOnInit() {
		this.titleService.setTitle(this.activatedRoute.snapshot.data.title);
	}

}





