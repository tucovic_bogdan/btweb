import { 
	Component, 
	OnInit,
	OnDestroy 
} from '@angular/core';

import { NgFor, NgIf } from '@angular/common';

import { fadeInAnimation } from '../../../animations/index';

import { 
	DataService, 
	sliderObj, 
	accordionObj 
} from './data.service';

@Component({
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css'],
	animations: [fadeInAnimation],
	providers: [
		DataService 
	],
  host: { '[@fadeInAnimation]': '' }
})

/**
 *  container komponenta za naslovnu stranicu
 */
export class HomeComponent implements OnInit, OnDestroy {

	// inputi za slider
	sliderItems: sliderObj[];
	sliderCurrentIndex: number = 0;
	slidesTotal: number = 0;

	// inputi za accordion
	accordionItems: accordionObj[];
	isAccordionItems: boolean = false;
	accordionCloseOthers: boolean = true;

	counterItems: { title : string, number : number }[];

	subscribeSLiderData;
	subscribeAccordionData;

	constructor(private dataService: DataService) {}

	ngOnInit() {
		this.subscribeSLiderData = this.dataService.getSliderData().subscribe((response) => {
			if (Array.isArray(response)) {
				this.sliderItems = Array.from(response);
				this.slidesTotal = this.sliderItems.length;
			} else {
				console.error('Podaci za slajder nisu uspesno preuzeti');
			} 
		});
		
		this.subscribeAccordionData = this.dataService.getAccordionData().subscribe((response) => {
			if (Array.isArray(response)) {
				this.accordionItems = Array.from(response);
				this.isAccordionItems = true;
			} else {
				console.error('Accordion podaci nisu uspesno preuzeti');
			}
		});
		
		this.counterItems = [
			{ title: 'Projekata' , number: 85 },
			{ title: 'Klijenata' , number: 63 },
			{ title: 'Država', number: 10 }
		]

	}

	ngOnDestroy() {
		this.subscribeSLiderData.unsubscribe();
		this.subscribeAccordionData.unsubscribe();
	}

	handleSlideChanged(data) {
		this.sliderCurrentIndex = data.nextIndex;
	}

}
