import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()

export class DataService {

	constructor (private http: HttpClient) { }

	getSliderData() {
		const apiUrl = 'https://api.myjson.com/bins/yz3il';
		return this.http.get(apiUrl).map(res => res);
	}

	getAccordionData() {
		const apiUrl = 'https://api.myjson.com/bins/17z66l';
		return this.http.get(apiUrl).map(res => res);
	}

}

export interface sliderObj {
	imgUrl: string,
	title: string
}

export interface accordionObj {
	id: number,
	title: string,
	content: string
}


















