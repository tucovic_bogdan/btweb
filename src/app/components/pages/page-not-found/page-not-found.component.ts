import { 
	Component
} from '@angular/core';

import { fadeInAnimation } from '../../../animations/index';

@Component({
	templateUrl: './page-not-found.component.html',
	styleUrls: ['./page-not-found.component.css'],
	animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': '' }
})


export class PageNotFoundComponent {



}
