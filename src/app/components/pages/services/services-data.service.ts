import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()

export class ServicesDataService {

	constructor (private http: HttpClient) { }

	getTabData() {
		const apiUrl = 'https://api.myjson.com/bins/g4xxp';
		
		// map operator ES7 novina :)
		return this.http.get(apiUrl).map(res => res);
	}
}





