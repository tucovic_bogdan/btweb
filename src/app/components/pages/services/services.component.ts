import { 
	Component,
	OnInit,
	EventEmitter,
	Output,
	OnDestroy 
} from '@angular/core';

import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

import { fadeInAnimation } from '../../../animations/index';

// interfejs za gallery object
import { galleryObject } from '../../../services/portfolio-gallery.service';

import { ServicesDataService } from './services-data.service';

@Component({
	templateUrl: './services.component.html',
	styleUrls: ['./services.component.css'],
	animations: [fadeInAnimation],
  host: { 
  	'[@fadeInAnimation]': '',
  },
  providers: [
  	ServicesDataService
  ]
})

/**
 *  container komponenta za services stranicu
 */
export class ServicesComponent implements OnInit, OnDestroy {
	
	galleryItems: galleryObject[]; 
	isGalleryItems: boolean = false;
	isTabItems: boolean = false;
	tabItems: { id: number, title: string, content: string }[];

	tabDataSubscribe;

	constructor( 
		private activatedRoute: ActivatedRoute,
		private dataService: ServicesDataService,
		private titleService: Title 
	) { }

	ngOnInit() {
		this.galleryItems = this.activatedRoute.snapshot.data.galleryItems;
		this.isGalleryItems = true;
		this.titleService.setTitle(this.activatedRoute.snapshot.data.title);

		this.tabDataSubscribe = this.dataService.getTabData().subscribe((response) => {
			if (Array.isArray(response)) {
				this.tabItems = Array.from(response);
				this.isTabItems = true;
			} else {
				console.error('Podaci o tabu nisu dostupni');
			}
		});

	}

	ngOnDestroy() {
		this.tabDataSubscribe.unsubscribe();
	}

}
