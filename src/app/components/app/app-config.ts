import { Injectable } from '@angular/core';

@Injectable()

export class AppConfig {
	
	// klasa teme 
	theme: 'theme--light' | 'theme--dark' = 'theme--light'; 

	/**
	 *  vrednost sirine prozora(px) ispod koje 
	 *  ce navigacija biti skrivena
	 */
	windowWhideNav: number = 1000;

}