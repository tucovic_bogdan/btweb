import { 
  Component,
  OnInit,
  Renderer2,
} from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

import { AppConfig } from './app-config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppConfig],
  animations: [
    trigger('navShowHide', [
      state('true', style({
        minWidth: '*'
      })),
      state('false', style({
        minWidth: '0', maxWidth: '0'
      })),
      transition('* <=> *', animate('500ms ease-out'))
    ])
  ]
})

// root komponenta
export class AppComponent implements OnInit {

  /**
   * navdedene teme su dostupne na osnovu SASS mape
   */
  currentTheme: 'theme--dark' | 'theme--light' ;
	
  // da li je navigacija otvorena ili skrivena
  navIsOpen: boolean;

  // sirina prozora(px) ispod koje je navigacija skrivena
  windowWhideNav: number;

	constructor ( private renderer: Renderer2, private config: AppConfig ) {}

  ngOnInit() {
    this.currentTheme = this.config.theme;
    this.windowWhideNav = this.config.windowWhideNav;

    this.setNavigationMode();
  }

  setTheme(newTheme: 'theme--dark' | 'theme--light'):void {
    this.currentTheme = newTheme;
  }

  setNavigationMode():void {
    this.renderer.listen('window', 'resize', (event) => {
      this.navIsOpen = this.getNavIsOpen(event.currentTarget);
    });

    this.renderer.listen('window', 'load', (event) => {
      this.navIsOpen = this.getNavIsOpen(event.currentTarget);
    });
  }

  getNavIsOpen(win):boolean {
    return win.innerWidth > this.windowWhideNav;
  }

}
