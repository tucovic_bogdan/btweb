import { NgModule } from '@angular/core'; 
import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module'; 
import { AppComponent } from './components/app/app.component'; 

// container komponente stranica
import { HomeComponent } from './components/pages/home/home.component'; 
import { ServicesComponent } from './components/pages/services/services.component';
import { PageNotFoundComponent  } from './components/pages/page-not-found/page-not-found.component';
import { ContactComponent } from './components/pages/contact/contact.component';

import { ContactFormComponent } from './components/pages/contact/contact-form/contact-form.component';

/**
 *  moduli za visestruku upotrebu
 *  bazirani na UI komponentama
 */
import { AccordionModule } from './components/shared/accordion/accordion.module'; 
import { CounterModule } from './components/shared/counter/counter.module';
import { SliderModule } from './components/shared/slider/slider.module';
import { ProgressbarModule } from './components/shared/progressbar/progressbar.module';
import { TabsModule } from './components/shared/tabs/tabs.module';
import { GalleryModule } from './components/shared/gallery/gallery.module';

// provajderi
import { PortfolioGalleryResolve } from './services/portfolio-gallery-resolve.service';
import { PortfolioGalleryService } from './services/portfolio-gallery.service';
import { WindowRef } from './services/window-ref.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ServicesComponent,
    ContactComponent,
    ContactFormComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    AccordionModule,
    CounterModule,
    SliderModule,
    ProgressbarModule,
    TabsModule,
    GalleryModule,
    AppRoutingModule
  ],
  providers:[
    PortfolioGalleryResolve,
    PortfolioGalleryService,
    WindowRef,
    Title
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
