import { NgModule } from '@angular/core'; 
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/pages/home/home.component'; 
import { ServicesComponent } from './components/pages/services/services.component'; 
import { ContactComponent } from './components/pages/contact/contact.component'; 
import { PageNotFoundComponent  } from './components/pages/page-not-found/page-not-found.component';
import { GalleryOpenImgComponent } from './components/shared/gallery/gallery-open-img.component'; 

import { PortfolioGalleryResolve } from './services/portfolio-gallery-resolve.service';

const appRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: 'usluge',
    component: ServicesComponent,
    data: { title: 'BTWEB | Usluge' },
    resolve: {
      galleryItems: PortfolioGalleryResolve
    }, 
    children: [
      { 
        path: 'galerija/:id', 
        component: GalleryOpenImgComponent,
        resolve: {
          galleryItems: PortfolioGalleryResolve
        } 
      }
    ]
  },
  {
    path: 'kontakt',
    component: ContactComponent,
    data: { title: 'BTWEB | Kontakt' }
  },
  { 
    path: '**', 
    component: PageNotFoundComponent 
  }
]
//najmanje precizna putanja dodaje se na kraju

@NgModule({
  imports: [
    RouterModule.forRoot( appRoutes )
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
