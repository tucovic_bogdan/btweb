import { trigger, state, animate, transition, style } from '@angular/animations';

export const fadeInAnimation =

  trigger('fadeInAnimation', [

    // animacija za ulazak elementa, void => *
    transition(':enter', [

      // css za pocetak naimacije
      style({ opacity: 0 }),

      // css za kraj animacije
      animate('300ms 100ms ease-out', style({ opacity: 1 }))
    ])
]);


