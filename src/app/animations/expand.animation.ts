import { trigger, state, animate, transition, style } from '@angular/animations';

export const expandAnimation = 

	trigger('expandAnimation', [
		transition('* <=> *', [
  		style({ transform: 'scale(0.95)' }),
      animate('300ms ease-out', style({ transform: 'scale(1)' }))
		])
	])