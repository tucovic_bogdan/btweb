var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var autoprefixer = require('gulp-autoprefixer');

var components = {
	sassSrc: './src/app/components/**/*.scss',
	cssDest: './src/app/components'
}

var globalStyle = {
	sassSrc: './src/styles/styles.scss',
	cssDest: './src'
}

var autoprefixerOptions = {
	browsers: ['last 4 versions']
}

var sassOptions = {
  outputStyle: 'expanded'
};

/**
 *  zadatak za stil komponenti
 *  stil komponenta deli zajednicke super globalne promenljive i mixine sa stilom komponenti
 *
 */ 
gulp.task('sassCompileComponents' , function () {
	return gulp.src(components.sassSrc)
		  .pipe(sass(sassOptions))
		  .pipe(autoprefixer(autoprefixerOptions))
		  .pipe(gulp.dest(components.cssDest))
});

// zadatak za globalni stil
gulp.task('sassCompileGlobalStyle' , function () {
	return gulp.src(globalStyle.sassSrc)
		  .pipe(sass(sassOptions))
		  .pipe(autoprefixer(autoprefixerOptions))
		  .pipe(gulp.dest(globalStyle.cssDest))
});


gulp.task('watch', function () {
	gulp.watch(components.sassSrc, ['sassCompileComponents'])
	gulp.watch('./src/styles/*.scss', ['sassCompileComponents', 'sassCompileGlobalStyle'])
	gulp.watch('./src/styles/*.scss', ['sassCompileGlobalStyle'])
	gulp.watch(globalStyle.sassSrc, ['sassCompileGlobalStyle'])
});

gulp.task('default', [
	'sassCompileComponents', 
	'watch'
	]
);



